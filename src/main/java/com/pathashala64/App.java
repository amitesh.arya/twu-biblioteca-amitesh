package com.pathashala64;

import com.pathashala64.controller.IO;
import com.pathashala64.controller.Menu;
import com.pathashala64.model.Book;
import com.pathashala64.model.Library;
import com.pathashala64.view.ConsoleIO;

import java.util.ArrayList;
import java.util.Arrays;

//Represents a library management System
class App {
    private static final String WELCOME_TO_BIBLIOTECA = "Welcome to Biblioteca...\n";

    private final IO ioDriver;
    private final Library library;
    private final Menu menu;


    App(IO ioDriver) {
        this(ioDriver, new Library(new ArrayList<Book>()));
    }

    App(IO ioDriver, Library library) {
        this.ioDriver = ioDriver;
        this.library = library;
        menu = new Menu(ioDriver, library);
    }

    public static App createApp() {
        Book headFirstJava = new Book("Head First JAVA", "Riely", 2000);
        Book conceptsOfOS = new Book("Concepts of OS", "Shreyas", 2005);
        Library newCollection = new Library(Arrays.asList(headFirstJava, conceptsOfOS));

        return new App(ConsoleIO.createConsoleIO(), newCollection);
    }

    void start() {
        welcome();
        menu.startMenu();
    }

    private void welcome() {
        ioDriver.display(WELCOME_TO_BIBLIOTECA);
    }
}
