package com.pathashala64;

public class AppStarter {
    public static void main(String[] args) {
        App bangalorePublicLibrary = App.createApp();

        bangalorePublicLibrary.start();
    }
}
