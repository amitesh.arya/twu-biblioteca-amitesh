package com.pathashala64.controller;

//Represents a contract for action on a menu option
public interface Actionable {
    void action();
}
