package com.pathashala64.controller;

import com.pathashala64.model.Book;
import com.pathashala64.model.Library;

import java.util.List;

//Represents Action of Book Display
public class BookDisplayAction implements Actionable {
    private static final String BOOK_NAME = "Book Name:";
    private static final String AUTHOR = "Author:";
    private static final String PUBLISHING_YEAR = "Publishing Year:";
    private static final String BOOK_LIST = "\nBook List\n";
    private static final String NO_BOOKS_AVAILABLE = "\nNo Books Available\n";

    private final Library books;
    private final IO ioDriver;

    public BookDisplayAction(Library books, IO ioDriver) {
        this.books = books;
        this.ioDriver = ioDriver;
    }

    public void action() {
        if (books.isEmpty()) {
            ioDriver.display(NO_BOOKS_AVAILABLE);
            return;
        }
        ioDriver.display(BOOK_LIST +
                "----------------------\n");
        List<Book> availableBooks = books.books();

        for (Book book : availableBooks) {
            ioDriver.display(BOOK_NAME + book.name() + "\n");
            ioDriver.display(AUTHOR + book.author() + "\n");
            ioDriver.display(PUBLISHING_YEAR + book.yearOfPublishing() + "\n----------------------" + "\n");
        }
    }
}
