package com.pathashala64.controller;

import com.pathashala64.model.BookNotIssuedException;
import com.pathashala64.model.Library;

//Represents Action of returning the book
public class BookReturnAction implements Actionable {
    public static final String PROMPT = ">>>(RETURN)";
    private static final String ENTER_MESSAGE = "\nEnter back to return to menu\nEnter the Book Name\n";
    private static final String INVALID_BOOK_NAME = "\nThat is not a valid book to return.\n";
    private static final String RETURN_MESSAGE = "\nThank you for returning the book.\n";
    private Library library;
    private IO ioDriver;

    public BookReturnAction(Library library, IO ioDriver) {
        this.library = library;
        this.ioDriver = ioDriver;
    }

    public void action() {
        ioDriver.display(ENTER_MESSAGE);
        ioDriver.display(PROMPT);
        String bookName = ioDriver.input();
        if (bookName.equalsIgnoreCase("back")) {
            return;
        }
        try {
            library.returns(bookName);
        } catch (BookNotIssuedException ignored) {
            ioDriver.display(INVALID_BOOK_NAME);
            return;
        }
        ioDriver.display(RETURN_MESSAGE);
    }
}
