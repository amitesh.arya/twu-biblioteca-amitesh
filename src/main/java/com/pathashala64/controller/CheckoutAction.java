package com.pathashala64.controller;

import com.pathashala64.model.BookNotAvailableException;
import com.pathashala64.model.Library;

//Represents Action of Checking Out the book
public class CheckoutAction implements Actionable {
    public static final String PROMPT = ">>>(CHECKOUT)";
    private static final String ENTER_MESSAGE = "\nEnter back to return to menu\nEnter Book Name To Checkout\n";
    private static final String NO_BOOKS_AVAILABLE = "\nNo Books Available for Checkout\n";
    private static final String CHECKOUT_MESSAGE = "\nThank you! Enjoy the book\n";
    private static final String BOOK_NOT_AVAILABLE = "\nThat book is not available.\n";
    private final Library books;
    private final IO ioDriver;

    public CheckoutAction(Library books, IO ioDriver) {
        this.books = books;
        this.ioDriver = ioDriver;
    }

    public void action() {
        if (books.isEmpty()) {
            ioDriver.display(NO_BOOKS_AVAILABLE);
            return;
        }
        ioDriver.display(ENTER_MESSAGE);
        ioDriver.display(PROMPT);
        String bookName = ioDriver.input();
        if (bookName.equalsIgnoreCase("back")) {
            return;
        }
        try {
            books.checkout(bookName);
        } catch (BookNotAvailableException bookNotAvailableException) {
            ioDriver.display(BOOK_NOT_AVAILABLE);
            return;
        }
        ioDriver.display(CHECKOUT_MESSAGE);
    }
}

