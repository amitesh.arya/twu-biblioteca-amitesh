package com.pathashala64.controller;

import com.pathashala64.model.Library;

import java.util.HashMap;
import java.util.Map;

public class Menu {
    private static final String INVALID_CHOICE_ENTERED = "\nINVALID CHOICE ENTERED...\n";
    private static final String PROMPT = ">>>(MENU)";

    private final IO ioDriver;
    private Map<String, Actionable> menuChoices;

    public Menu(IO ioDriver, Library library) {
        this.ioDriver = ioDriver;
        menuChoices = new HashMap<>();
        menuChoices.put("list", new BookDisplayAction(library, ioDriver));
        menuChoices.put("checkout", new CheckoutAction(library, ioDriver));
        menuChoices.put("return", new BookReturnAction(library, ioDriver));
        menuChoices.put("quit", new QuitAction(ioDriver));
    }

    public void startMenu() {
        String choice = "a";
        while (!choice.equals("quit")) {
            ioDriver.display("\nMENU\nEnter 'list' to view List of Books\nEnter 'checkout' for checkout book\nEnter 'return' for Returning book\nEnter 'quit' for exit\nEnter your choice\n");
            ioDriver.display(PROMPT);
            choice = ioDriver.input();
            executeChoice(choice);
        }
    }

    void executeChoice(String choice) {
        choice = choice.toLowerCase();
        if (menuChoices.containsKey(choice)) {
            Actionable choiceEntered = menuChoices.get(choice);
            choiceEntered.action();
            return;
        }
        ioDriver.display(INVALID_CHOICE_ENTERED);
    }


}
