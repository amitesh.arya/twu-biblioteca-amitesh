package com.pathashala64.controller;

//Represents quitting Action
public class QuitAction implements Actionable {
    private IO ioDriver;

    public QuitAction(IO ioDriver) {
        this.ioDriver = ioDriver;
    }

    public void action() {
        ioDriver.display("\n*****    **   **   *****\n");
        ioDriver.display("**  **    ** **    **   \n");
        ioDriver.display("*****      **      *****\n");
        ioDriver.display("**  **     **      **   \n");
        ioDriver.display("*****      **      *****\n");
    }
}
