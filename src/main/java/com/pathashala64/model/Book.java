package com.pathashala64.model;

//Represents a bound set of written or printed work
public class Book {
    private final String name;
    private final String author;
    private final int yearOfPublish;

    public Book(String name, String author, int yearOfPublish) {
        this.name = name;
        this.author = author;
        this.yearOfPublish = yearOfPublish;
    }

    public String name() {
        return name;
    }

    public String author() {
        return author;
    }

    public int yearOfPublishing() {
        return yearOfPublish;
    }

    public boolean compareName(String otherBookName) {
        return name.equalsIgnoreCase(otherBookName);
    }
}
