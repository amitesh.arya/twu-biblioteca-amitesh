package com.pathashala64.model;

//Represents a situation when book is not available
public class BookNotAvailableException extends RuntimeException {
    public BookNotAvailableException() {
        super();
    }
}
