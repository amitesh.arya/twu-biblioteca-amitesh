package com.pathashala64.model;

//Represents a situation when the book has not been issued
public class BookNotIssuedException extends RuntimeException {
    public BookNotIssuedException() {
        super();
    }
}
