package com.pathashala64.model;

import java.util.ArrayList;
import java.util.List;

//Represents a set of books
public class Library {
    private final List<Book> availableBooks;
    private final List<Book> checkedOutBooks;

    public Library(List<Book> availableBooks) {
        this.availableBooks = new ArrayList<>(availableBooks);
        checkedOutBooks = new ArrayList<>();
    }

    public Library() {
        availableBooks = new ArrayList<>();
        checkedOutBooks = new ArrayList<>();
    }

    public List<Book> books() {
        return availableBooks;
    }

    public boolean isEmpty() {
        return availableBooks.isEmpty();
    }

    public void checkout(String bookName) {
        if (notAvailable(bookName, availableBooks)) {
            throw new BookNotAvailableException();
        }
        changeBookStatus(bookName, availableBooks, checkedOutBooks);
    }

    public void returns(String bookName) {
        if (notAvailable(bookName, checkedOutBooks)) {
            throw new BookNotIssuedException();
        }
        changeBookStatus(bookName, checkedOutBooks, availableBooks);
    }

    boolean notAvailable(String bookName, List<Book> books) {
        for (Book book : books) {
            if (book.compareName(bookName)) {
                return false;
            }
        }
        return true;
    }

    private void changeBookStatus(String bookName, List<Book> addBooks, List<Book> removeBooks) {
        for (Book book : addBooks) {
            if (book.compareName(bookName)) {
                addBooks.remove(book);
                removeBooks.add(book);
                return;
            }
        }
    }
}
