package com.pathashala64.view;

import com.pathashala64.controller.IO;

import java.io.PrintStream;
import java.util.Scanner;

public class ConsoleIO implements IO {
    private final Scanner inStream;
    private final PrintStream outStream;

    public ConsoleIO(Scanner inStream, PrintStream outStream) {
        this.inStream = inStream;
        this.outStream = outStream;
    }

    public static ConsoleIO createConsoleIO() {
        Scanner scanner = new Scanner(System.in);
        return new ConsoleIO(scanner, System.out);
    }

    @Override
    public void display(String message) {
        outStream.print(message);
    }

    @Override
    public String input() {
        return inStream.nextLine();
    }
}
