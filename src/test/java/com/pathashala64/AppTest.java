package com.pathashala64;

import com.pathashala64.controller.IO;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

class AppTest {
    @Test
    void expectsWelcomeMessageOnStartUp() {
        IO mockIO = mock(IO.class);
        App bangalorePublicLibrary = new App(mockIO);
        when(mockIO.input()).thenReturn("quit");
        bangalorePublicLibrary.start();

        verify(mockIO).display("Welcome to Biblioteca...\n");
    }
}
