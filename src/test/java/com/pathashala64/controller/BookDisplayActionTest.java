package com.pathashala64.controller;

import com.pathashala64.model.Book;
import com.pathashala64.model.Library;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class BookDisplayActionTest {
    @Test
    void expectsDisplayingBooksInBookCollection() {
        IO mockIO = mock(IO.class);
        List<Book> bookList = new ArrayList<>();
        bookList.add(new Book("Harry Potter Part3", "J K Rowling", 2005));
        Actionable displayAction = new BookDisplayAction(new Library(bookList), mockIO);

        displayAction.action();

        verify(mockIO).display("\nBook List\n" +
                "----------------------\n");
        verify(mockIO).display("Book Name:Harry Potter Part3\n");
        verify(mockIO).display("Author:J K Rowling\n");
        verify(mockIO).display("Publishing Year:2005\n----------------------\n");

    }

    @Test
    void expectsToDisplayNoBooksAvailableWhenBookListEmpty() {
        IO mockIO = mock(IO.class);
        BookDisplayAction newAction = new BookDisplayAction(new Library(), mockIO);

        newAction.action();

        verify(mockIO).display("\nNo Books Available\n");
    }

}
