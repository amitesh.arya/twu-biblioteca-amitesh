package com.pathashala64.controller;

import com.pathashala64.model.Book;
import com.pathashala64.model.Library;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.*;

class BookReturnActionTest {
    @Test
    void expectsBookReturnSuccessfulWhenBookHasCheckedOut() {
        IO mockIO = mock(IO.class);
        Book ConceptsOfOS = new Book("Concepts of OS", "Galvin", 2005);
        Library newCollection = new Library(Arrays.asList(ConceptsOfOS));
        newCollection.checkout("Concepts of OS");
        when(mockIO.input()).thenReturn("Concepts of OS");
        BookReturnAction newAction = new BookReturnAction(newCollection, mockIO);

        newAction.action();

        assertFalse(newCollection.isEmpty());
    }

    @Test
    void expectsBookReturnUnsuccessfulWhenBookHasNotCheckedOut() {
        IO mockIO = mock(IO.class);
        Book ConceptsOfOS = new Book("Concepts of OS", "Galvin", 2005);
        Book headFirstJava = new Book("Head First JAVA", "Riely", 2000);
        Library newCollection = new Library(Arrays.asList(headFirstJava, ConceptsOfOS));
        when(mockIO.input()).thenReturn("Concepts of OS");
        BookReturnAction newAction = new BookReturnAction(newCollection, mockIO);

        newAction.action();

        verify(mockIO).display("\nEnter back to return to menu\nEnter the Book Name\n");
        verify(mockIO).display(">>>(RETURN)");
        verify(mockIO).display("\nThat is not a valid book to return.\n");
    }

    @Test
    void expectsToReturnWhenBackIsEntered() {
        IO mockIO = mock(IO.class);
        Book ConceptsOfOS = new Book("Concepts of OS", "Galvin", 2005);
        Book headFirstJava = new Book("Head First JAVA", "Riely", 2000);
        Library newCollection = new Library(Arrays.asList(headFirstJava, ConceptsOfOS));
        when(mockIO.input()).thenReturn("back");
        BookReturnAction newAction = new BookReturnAction(newCollection, mockIO);

        newAction.action();

        verify(mockIO, times(2)).display(anyString());

    }

    @Test
    void expectsToReturnWhenBACKIsEntered() {
        IO mockIO = mock(IO.class);
        Book ConceptsOfOS = new Book("Concepts of OS", "Galvin", 2005);
        Book headFirstJava = new Book("Head First JAVA", "Riely", 2000);
        Library newCollection = new Library(Arrays.asList(headFirstJava, ConceptsOfOS));
        when(mockIO.input()).thenReturn("BACK");
        BookReturnAction newAction = new BookReturnAction(newCollection, mockIO);

        newAction.action();

        verify(mockIO, times(2)).display(anyString());

    }
}