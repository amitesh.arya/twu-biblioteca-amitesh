package com.pathashala64.controller;

import com.pathashala64.model.Book;
import com.pathashala64.model.Library;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.mockito.Mockito.*;

class CheckoutActionTest {

    @Test
    void expectsToDisplayNoBooksAvailableWhenBookListEmpty() {
        IO mockIO = mock(IO.class);
        CheckoutAction newAction = new CheckoutAction(new Library(), mockIO);

        newAction.action();

        verify(mockIO).display("\nNo Books Available for Checkout\n");
    }

    @Test
    void expectsToSuccessfullyCheckoutBookWhenBookIsAvailable() {
        IO mockIO = mock(IO.class);
        Book headFirstJava = new Book("Head First JAVA", "Riely", 2000);
        Book ConceptsOfOS = new Book("Concepts of OS", "Galvin", 2005);
        Library newCollection = new Library(Arrays.asList(headFirstJava, ConceptsOfOS));
        CheckoutAction newAction = new CheckoutAction(newCollection, mockIO);
        when(mockIO.input()).thenReturn("Head First JAVA");

        newAction.action();

        verify(mockIO).display("\nEnter back to return to menu\nEnter Book Name To Checkout\n");
        verify(mockIO).display(">>>(CHECKOUT)");
        verify(mockIO).display("\nThank you! Enjoy the book\n");
    }

    @Test
    void expectsToCheckoutUnsuccessfulWhenBookIsNotAvailable() {
        IO mockIO = mock(IO.class);
        Book headFirstJava = new Book("Head First JAVA", "Riely", 2000);
        Book ConceptsOfOS = new Book("Concepts of OS", "Galvin", 2005);
        Library newCollection = new Library(Arrays.asList(headFirstJava, ConceptsOfOS));
        CheckoutAction newAction = new CheckoutAction(newCollection, mockIO);
        when(mockIO.input()).thenReturn("Head First JAVA1234");

        newAction.action();

        verify(mockIO).display("\nEnter back to return to menu\nEnter Book Name To Checkout\n");
        verify(mockIO).display(">>>(CHECKOUT)");
        verify(mockIO).display("\nThat book is not available.\n");
    }

    @Test
    void expectsToReturnToMenuWhenBackIsEntered() {
        IO mockIO = mock(IO.class);
        Book headFirstJava = new Book("Head First JAVA", "Riely", 2000);
        Book ConceptsOfOS = new Book("Concepts of OS", "Galvin", 2005);
        Library newCollection = new Library(Arrays.asList(headFirstJava, ConceptsOfOS));
        CheckoutAction newAction = new CheckoutAction(newCollection, mockIO);
        when(mockIO.input()).thenReturn("back");

        newAction.action();

        verify(mockIO, times(2)).display(anyString());
    }

    @Test
    void expectsToReturnToMenuWhenBACKIsEntered() {
        IO mockIO = mock(IO.class);
        Book headFirstJava = new Book("Head First JAVA", "Riely", 2000);
        Book ConceptsOfOS = new Book("Concepts of OS", "Galvin", 2005);
        Library newCollection = new Library(Arrays.asList(headFirstJava, ConceptsOfOS));
        CheckoutAction newAction = new CheckoutAction(newCollection, mockIO);
        when(mockIO.input()).thenReturn("BACK");

        newAction.action();

        verify(mockIO, times(2)).display(anyString());
    }
}