package com.pathashala64.controller;


import com.pathashala64.model.Book;
import com.pathashala64.model.Library;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

public class MenuTest {
    @Test
    void expectsToDisplayBookListWhenUserEntersChoiceForBookList() {
        IO mockIO = mock(IO.class);
        List<Book> bookList = new ArrayList<>();
        bookList.add(new Book("Harry Potter Part3", "J K Rowling", 2005));
        Menu menu = new Menu(mockIO, new Library(bookList));
        ArgumentCaptor<String> messageCaptor = ArgumentCaptor.forClass(String.class);

        when(mockIO.input()).thenReturn("list", "quit");
        menu.startMenu();

        verify(mockIO, times(13)).display(messageCaptor.capture());
        assertEquals("\nMENU\nEnter 'list' to view List of Books\nEnter 'checkout' for checkout book\nEnter 'return' for Returning book\nEnter 'quit' for exit\nEnter your choice\n", messageCaptor.getAllValues().get(0));
        assertEquals(">>>(MENU)", messageCaptor.getAllValues().get(1));
        assertEquals("\nBook List\n----------------------\n", messageCaptor.getAllValues().get(2));
        assertEquals("Book Name:Harry Potter Part3\n", messageCaptor.getAllValues().get(3));
        assertEquals("Author:J K Rowling\n", messageCaptor.getAllValues().get(4));
        assertEquals("Publishing Year:2005\n----------------------\n", messageCaptor.getAllValues().get(5));
        assertEquals("\nMENU\nEnter 'list' to view List of Books\nEnter 'checkout' for checkout book\nEnter 'return' for Returning book\nEnter 'quit' for exit\nEnter your choice\n", messageCaptor.getAllValues().get(6));
        verify(mockIO, times(2)).input();
    }

    @Test
    void expectsToDisplayInvalidChoiceWhenUserEntersInvalidChoice() {
        IO mockIO = mock(IO.class);
        List<Book> bookList = new ArrayList<>();
        bookList.add(new Book("Harry Potter Part3", "J K Rowling", 2005));
        Menu menu = new Menu(mockIO, new Library(bookList));

        ArgumentCaptor<String> messageCaptor = ArgumentCaptor.forClass(String.class);

        when(mockIO.input()).thenReturn("10", "quit");
        menu.startMenu();

        verify(mockIO, times(10)).display(messageCaptor.capture());
        assertEquals("\nINVALID CHOICE ENTERED...\n", messageCaptor.getAllValues().get(2));
        verify(mockIO, times(2)).input();
    }

    @Test
    void expectsToCheckOutBookWhenUserEntersCheckoutBook() {
        IO mockIO = mock(IO.class);
        List<Book> bookList = new ArrayList<>();
        bookList.add(new Book("Harry Potter Part3", "J K Rowling", 2005));
        Menu menu = new Menu(mockIO, new Library(bookList));
        ArgumentCaptor<String> messageCaptor = ArgumentCaptor.forClass(String.class);

        when(mockIO.input()).thenReturn("checkout", "Harry Potter Part3", "quit");
        menu.startMenu();

        verify(mockIO, times(12)).display(messageCaptor.capture());
        assertEquals("\nMENU\nEnter 'list' to view List of Books\nEnter 'checkout' for checkout book\nEnter 'return' for Returning book\nEnter 'quit' for exit\nEnter your choice\n", messageCaptor.getAllValues().get(0));
        assertEquals(">>>(MENU)", messageCaptor.getAllValues().get(1));
        assertEquals("\nEnter back to return to menu\nEnter Book Name To Checkout\n", messageCaptor.getAllValues().get(2));
        assertEquals(">>>(CHECKOUT)", messageCaptor.getAllValues().get(3));
        assertEquals("\nThank you! Enjoy the book\n", messageCaptor.getAllValues().get(4));
        assertEquals("\nMENU\nEnter 'list' to view List of Books\nEnter 'checkout' for checkout book\nEnter 'return' for Returning book\nEnter 'quit' for exit\nEnter your choice\n", messageCaptor.getAllValues().get(5));
        verify(mockIO, times(3)).input();
    }

    @Test
    void expectsToReturnBookWhenUserEntersReturnBook() {
        IO mockIO = mock(IO.class);
        List<Book> bookList = new ArrayList<>();
        bookList.add(new Book("Harry Potter Part3", "J K Rowling", 2005));
        Menu menu = new Menu(mockIO, new Library(bookList));
        ArgumentCaptor<String> messageCaptor = ArgumentCaptor.forClass(String.class);

        when(mockIO.input()).thenReturn("return", "Harry Potter Part3", "quit");
        menu.startMenu();

        verify(mockIO, times(12)).display(messageCaptor.capture());
        assertEquals("\nMENU\nEnter 'list' to view List of Books\nEnter 'checkout' for checkout book\nEnter 'return' for Returning book\nEnter 'quit' for exit\nEnter your choice\n", messageCaptor.getAllValues().get(0));
        assertEquals(">>>(MENU)", messageCaptor.getAllValues().get(1));
        assertEquals("\nEnter back to return to menu\nEnter the Book Name\n", messageCaptor.getAllValues().get(2));
        assertEquals(">>>(RETURN)", messageCaptor.getAllValues().get(3));
        assertEquals("\nThat is not a valid book to return.\n", messageCaptor.getAllValues().get(4));
        assertEquals("\nMENU\nEnter 'list' to view List of Books\nEnter 'checkout' for checkout book\nEnter 'return' for Returning book\nEnter 'quit' for exit\nEnter your choice\n", messageCaptor.getAllValues().get(5));
        verify(mockIO, times(3)).input();
    }

    @Test
    void expectsToQuitBookWhenUserEntersQuit() {
        IO mockIO = mock(IO.class);
        List<Book> bookList = new ArrayList<>();
        bookList.add(new Book("Harry Potter Part3", "J K Rowling", 2005));
        Menu menu = new Menu(mockIO, new Library(bookList));
        ArgumentCaptor<String> messageCaptor = ArgumentCaptor.forClass(String.class);

        when(mockIO.input()).thenReturn("quit");
        menu.startMenu();

        verify(mockIO, times(7)).display(messageCaptor.capture());
        assertEquals("\nMENU\nEnter 'list' to view List of Books\nEnter 'checkout' for checkout book\nEnter 'return' for Returning book\nEnter 'quit' for exit\nEnter your choice\n", messageCaptor.getAllValues().get(0));
        assertEquals(">>>(MENU)", messageCaptor.getAllValues().get(1));
        verify(mockIO, times(1)).input();
    }

    @Test
    void expectsQUITToMapWithQuitAction(){
        IO mockIO = mock(IO.class);
        List<Book> bookList = new ArrayList<>();
        bookList.add(new Book("Harry Potter Part3", "J K Rowling", 2005));
        Menu menu = new Menu(mockIO, new Library(bookList));
        ArgumentCaptor<String> messageCaptor = ArgumentCaptor.forClass(String.class);

        menu.executeChoice("QUIT");

        verify(mockIO, times(5)).display(messageCaptor.capture());
    }

    @Test
    void expectsLISTToMapWithDisplayList() {
        IO mockIO = mock(IO.class);
        List<Book> bookList = new ArrayList<>();
        bookList.add(new Book("Harry Potter Part3", "J K Rowling", 2005));
        Menu menu = new Menu(mockIO, new Library(bookList));
        ArgumentCaptor<String> messageCaptor = ArgumentCaptor.forClass(String.class);

        when(mockIO.input()).thenReturn("LIST", "quit");
        menu.startMenu();

        verify(mockIO, times(13)).display(messageCaptor.capture());
        assertEquals("\nBook List\n----------------------\n", messageCaptor.getAllValues().get(2));
        assertEquals("Book Name:Harry Potter Part3\n", messageCaptor.getAllValues().get(3));
        assertEquals("Author:J K Rowling\n", messageCaptor.getAllValues().get(4));
        assertEquals("Publishing Year:2005\n----------------------\n", messageCaptor.getAllValues().get(5));
    }

    @Test
    void expectsCHECKOUTToMapWithCheckoutAction() {
        IO mockIO = mock(IO.class);
        List<Book> bookList = new ArrayList<>();
        bookList.add(new Book("Harry Potter Part3", "J K Rowling", 2005));
        Menu menu = new Menu(mockIO, new Library(bookList));
        ArgumentCaptor<String> messageCaptor = ArgumentCaptor.forClass(String.class);

        when(mockIO.input()).thenReturn("Harry Potter Part3");
        menu.executeChoice("CHECKOUT");

        verify(mockIO, times(3)).display(messageCaptor.capture());
        assertEquals("\nThank you! Enjoy the book\n", messageCaptor.getAllValues().get(2));
    }

    @Test
    void expectsRETURNToMapWithReturnAction() {
        IO mockIO = mock(IO.class);
        List<Book> bookList = new ArrayList<>();
        bookList.add(new Book("Harry Potter Part3", "J K Rowling", 2005));
        Menu menu = new Menu(mockIO, new Library(bookList));
        ArgumentCaptor<String> messageCaptor = ArgumentCaptor.forClass(String.class);

        when(mockIO.input()).thenReturn("Harry Potter Part3");
        menu.executeChoice("RETURN");

        verify(mockIO, times(3)).display(messageCaptor.capture());
        assertEquals("\nEnter back to return to menu\nEnter the Book Name\n", messageCaptor.getAllValues().get(0));
        assertEquals(">>>(RETURN)", messageCaptor.getAllValues().get(1));
        assertEquals("\nThat is not a valid book to return.\n", messageCaptor.getAllValues().get(2));
    }

    @Test
    void expectsQuitToMapWithQuitAction(){
        IO mockIO = mock(IO.class);
        List<Book> bookList = new ArrayList<>();
        bookList.add(new Book("Harry Potter Part3", "J K Rowling", 2005));
        Menu menu = new Menu(mockIO, new Library(bookList));
        ArgumentCaptor<String> messageCaptor = ArgumentCaptor.forClass(String.class);

        menu.executeChoice("quit");

        verify(mockIO, times(5)).display(messageCaptor.capture());
    }

    @Test
    void expectsListToMapWithDisplayList() {
        IO mockIO = mock(IO.class);
        List<Book> bookList = new ArrayList<>();
        bookList.add(new Book("Harry Potter Part3", "J K Rowling", 2005));
        Menu menu = new Menu(mockIO, new Library(bookList));
        ArgumentCaptor<String> messageCaptor = ArgumentCaptor.forClass(String.class);

        when(mockIO.input()).thenReturn("list", "quit");
        menu.startMenu();

        verify(mockIO, times(13)).display(messageCaptor.capture());
        assertEquals("\nBook List\n----------------------\n", messageCaptor.getAllValues().get(2));
        assertEquals("Book Name:Harry Potter Part3\n", messageCaptor.getAllValues().get(3));
        assertEquals("Author:J K Rowling\n", messageCaptor.getAllValues().get(4));
        assertEquals("Publishing Year:2005\n----------------------\n", messageCaptor.getAllValues().get(5));
    }

    @Test
    void expectsInvalidChoiceToBeShownWhenInvalidChoiceEntered() {
        IO mockIO = mock(IO.class);
        List<Book> bookList = new ArrayList<>();
        bookList.add(new Book("Harry Potter Part3", "J K Rowling", 2005));
        Menu menu = new Menu(mockIO, new Library(bookList));

        menu.executeChoice("asdf");

        verify(mockIO).display("\nINVALID CHOICE ENTERED...\n");
    }

    @Test
    void expectscheckoutToMapWithCheckoutAction() {
        IO mockIO = mock(IO.class);
        List<Book> bookList = new ArrayList<>();
        bookList.add(new Book("Harry Potter Part3", "J K Rowling", 2005));
        Menu menu = new Menu(mockIO, new Library(bookList));
        ArgumentCaptor<String> messageCaptor = ArgumentCaptor.forClass(String.class);

        when(mockIO.input()).thenReturn("Harry Potter Part3");
        menu.executeChoice("checkout");

        verify(mockIO, times(3)).display(messageCaptor.capture());
        assertEquals("\nThank you! Enjoy the book\n", messageCaptor.getAllValues().get(2));
    }

    @Test
    void expectsReturnToMapWithReturnAction() {
        IO mockIO = mock(IO.class);
        List<Book> bookList = new ArrayList<>();
        bookList.add(new Book("Harry Potter Part3", "J K Rowling", 2005));
        Menu menu = new Menu(mockIO, new Library(bookList));
        ArgumentCaptor<String> messageCaptor = ArgumentCaptor.forClass(String.class);

        when(mockIO.input()).thenReturn("Harry Potter Part3");
        menu.executeChoice("return");

        verify(mockIO, times(3)).display(messageCaptor.capture());
        assertEquals("\nEnter back to return to menu\nEnter the Book Name\n", messageCaptor.getAllValues().get(0));
        assertEquals(">>>(RETURN)", messageCaptor.getAllValues().get(1));
        assertEquals("\nThat is not a valid book to return.\n", messageCaptor.getAllValues().get(2));
    }
}
