package com.pathashala64.controller;

import com.pathashala64.view.ConsoleIO;
import org.junit.jupiter.api.Test;

import static org.mockito.Mockito.*;

public class QuitTest {
    @Test
    void expectsExitMessageToBePrintedOnExit() {
        IO mockIO = mock(ConsoleIO.class);
        Actionable newAction = new QuitAction(mockIO);

        newAction.action();

        verify(mockIO).display("\n*****    **   **   *****\n");
        verify(mockIO).display("**  **    ** **    **   \n");
        verify(mockIO, times(2)).display("*****      **      *****\n");
        verify(mockIO).display("**  **     **      **   \n");
    }
}
