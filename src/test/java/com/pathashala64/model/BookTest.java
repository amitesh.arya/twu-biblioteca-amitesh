package com.pathashala64.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BookTest {

    @Test
    void expectsHeadFirstJavaWhenQueryingForBookName() {
        Book headFirst = new Book("Head First Java", "Rieley", 2000);
        assertEquals("Head First Java", headFirst.name());

    }

    @Test
    void expectsGalvinWhenQueryingForBookAuthor() {
        Book conceptOfOS = new Book("Concept of OS", "Galvin", 2005);
        assertEquals("Galvin", conceptOfOS.author());
    }

    @Test
    void expects2005WHrnQueryingForBookPublishYear() {
        Book conceptOfOS = new Book("Concept of OS", "Galvin", 2005);
        assertEquals(2005, conceptOfOS.yearOfPublishing());
    }

    @Test
    void expectsTrueWhenBookNameSameAsNameEntered() {
        Book conceptOfOS = new Book("Concept of OS", "Galvin", 2005);
        assertTrue(conceptOfOS.compareName("Concept of OS"));
    }

    @Test
    void expectsFalseWhenBookNameDifferentAsNameEntered() {
        Book conceptOfOS = new Book("Concept of OS", "Galvin", 2005);
        assertFalse(conceptOfOS.compareName("Concept of OSasdf"));
    }
}
