package com.pathashala64.model;

import com.pathashala64.controller.IO;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

class LibraryTest {

    @Test
    void expectsToCheckOutBookFromAvailableBook() {
        Book headFirstJava = new Book("Head First JAVA", "Riely", 2000);
        Book ConceptsOfOS = new Book("Concepts of OS", "Galvin", 2005);
        Library newCollection = new Library(Arrays.asList(headFirstJava, ConceptsOfOS));
        IO mockIO = mock(IO.class);

        assertDoesNotThrow(() -> newCollection.checkout("Head First JAVA"));
    }

    @Test
    void expectsToShowUnsuccessfulCheckOutBookFromAvailableBook() {
        Book headFirstJava = new Book("Head First JAVA", "Riely", 2000);
        Book ConceptsOfOS = new Book("Concepts of OS", "Galvin", 2005);
        Library newCollection = new Library(Arrays.asList(headFirstJava, ConceptsOfOS));
        IO mockIO = mock(IO.class);

        assertThrows(BookNotAvailableException.class, () -> newCollection.checkout("Concepts of OSasd"));
    }

    @Test
    void expectsToReturnBookWhenBookHasBeenIssued() {
        Book headFirstJava = new Book("Head First JAVA", "Riely", 2000);
        Book ConceptsOfOS = new Book("Concepts of OS", "Galvin", 2005);
        Library newCollection = new Library(Arrays.asList(headFirstJava, ConceptsOfOS));
        IO mockIO = mock(IO.class);

        newCollection.checkout("Head First JAVA");

        assertDoesNotThrow(() -> newCollection.returns("Head First JAVA"));
    }

    @Test
    void expectsBookNotIssuedExceptionBookWhenBookHasNotBeenIssuedAndAskedReturn() {
        Book headFirstJava = new Book("Head First JAVA", "Riely", 2000);
        Book ConceptsOfOS = new Book("Concepts of OS", "Galvin", 2005);
        Library newCollection = new Library(Arrays.asList(headFirstJava, ConceptsOfOS));
        IO mockIO = mock(IO.class);

        newCollection.checkout("Head First JAVA");

        assertThrows(BookNotIssuedException.class, () -> newCollection.returns("Concepts of OSasd"));
    }

    @Test
    void expectsTrueWhenBookNotAvailableInList() {
        Book headFirstJava = new Book("Head First JAVA", "Riely", 2000);
        Book ConceptsOfOS = new Book("Concepts of OS", "Galvin", 2005);
        Library newCollection = new Library(Arrays.asList(headFirstJava, ConceptsOfOS));
        IO mockIO = mock(IO.class);

        assertTrue(newCollection.notAvailable("asdf", Arrays.asList(headFirstJava, ConceptsOfOS)));
    }

    @Test
    void expectsFalseWhenBookAvailableInList() {
        Book headFirstJava = new Book("Head First JAVA", "Riely", 2000);
        Book ConceptsOfOS = new Book("Concepts of OS", "Galvin", 2005);
        Library newCollection = new Library(Arrays.asList(headFirstJava, ConceptsOfOS));
        IO mockIO = mock(IO.class);

        assertFalse(newCollection.notAvailable("Concepts of OS", Arrays.asList(headFirstJava, ConceptsOfOS)));
    }
}
