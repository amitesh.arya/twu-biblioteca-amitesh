package com.pathashala64.view;

import org.junit.jupiter.api.Test;

import java.io.PrintStream;
import java.util.Scanner;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

class ConsoleIOTest {
    @Test
    void expectsToDisplayMessage() {
        Scanner scanner = new Scanner(System.in);
        PrintStream mockPrintStream = mock(PrintStream.class);
        ConsoleIO newConsoleIOObject = new ConsoleIO(scanner, mockPrintStream);

        newConsoleIOObject.display("This is a mock");

        verify(mockPrintStream).print("This is a mock");
    }
}
